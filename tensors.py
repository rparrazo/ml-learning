#!/usr/bin/python3

import torch

#manual seeding
torch.manual_seed(1234)
z = torch.rand(2, 3)
print("Random 2x3 tensor")
print(z)
print()

print("New random tensor")
y = torch.rand(2, 3)
print(y)
print()

print("re-seed, should match first")
torch.manual_seed(1234)
x = torch.rand(2, 3)
print(x)
print()

print("Print element 0,2:")
print (x[0,2])
print()

print("ones tensor:")
ones = torch.ones(2, 3)
print(ones)
print()

print("twos tensor:")
twos=torch.ones(2,3)*2
print(twos)
print()

# alternative just multiply ones tensor by 2
#print("twos tensor:")
#print (ones * 2)

print("Add ones + twos tensor together:")
threes = ones + twos
print (threes)
print(threes.shape)
print()


